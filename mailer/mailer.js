const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');

let mailConfig;
if (process.env.NODE_ENV === 'production') {
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_SECRET
        }
    }
    mailConfig = sgTransport(options);
} else {
    if (process.env.NODE_ENV === 'staging') {
        console.log('Ambiente de desarrollo');
        const options = {
            auth: {
                api_key: process.env.SENDGRID_API_SECRET
            }
        }
    } else {
        mailConfig = {
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: process.env.ethereal_user,
                pass: process.env.ethereal_pwd
            }
        };
    }
};

/*const mailConfig ={
    host: 'smtp.ethereal.email',
    port: 587,
    auth: {
        user: 'daija5@ethereal.email',
        pass: 'hcm2py2SH7YBhR4REh'
    }
};*/

module.exports = nodemailer.createTransport(mailConfig);
