var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var base_url = "http://localhost:3000/api/bicicletas";

describe('Testing Bicicletas', function() {
    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            done();
        });
    });
    
    describe('Bicicleta API', () => {
        describe('GET BICICLETAS /', () => {
            it('STATUS 200', (done) => {
                Bicicleta.allBicis(function(err, bicis) {
                    expect(bicis.length).toBe(0);
                });
                
                request.get(base_url, function(error, response, body) {
                    expect(response.statusCode).toBe(200);
                });
    
                done();
            });
        });
    });

    describe('POST BICICLETAS /CREATE', () => {
        it('STATUS 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "code": 10, "color": "rojo", "modelo": "urbana", "lat": -34.5, "lng": -54.1 }';
    
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(201);
                Bicicleta.findByCode(10, function(error, targetBici) {
                    expect(targetBici.code).toBe(10);
                    expect(targetBici.color).toBe("rojo");
                    expect(targetBici.modelo).toBe("urbana");
                    done();
                });
            });
        });
    });

    describe('POST BICICLETAS /UPDATE', () => {
        it('STATUS 203', (done) => {
            var a = Bicicleta.createInstance(10, "rojo", "urbana", [-34.6012424, -58.3861497]);
            Bicicleta.add(a);
    
            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "code": 10, "color": "dorado", "modelo": "montaña", "lat": -34.5, "lng": -54.1 }';
    
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/update',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(203);
                
                Bicicleta.findByCode(10, function(error, targetBici) {
                    expect(targetBici.code).toBe(10);
                    expect(targetBici.color).toBe("dorado");
                    expect(targetBici.modelo).toBe("montaña");
                    done();
                });
            });
        });
    });

    describe('POST BICICLETAS /DELETE', () => {
        it('STATUS 204', (done) => {
            var a = Bicicleta.createInstance(1, "rojo", "urbana", [-34.6012424, -58.3861497]);
            var b = Bicicleta.createInstance(2, "blanca", "urbana", [-34.596932, -58.3808287]);
            
            Bicicleta.add(a);
            Bicicleta.add(b);
    
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"code": 1}';
    
            request.delete({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/delete',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(204);
                Bicicleta.allBicis(function(err, bicis) {
                    expect(bicis.length).toBe(1);
                });
                done();
            });
        });
    });
});