var map = L.map('map').setView([-34.7025200, -58.3955000], 14);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

/*L.marker([-34.6012424,-58.3861497]).addTo(map);
L.marker([-34.596932,-58.3808287]).addTo(map);
L.marker([-34.599564,-58.3778777]).addTo(map);*/

/*$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result) {
        console.log(result);
        result.bicicletas.forEach(function (bici) {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})*/

$.ajax({
    method: 'POST',
    dataType: 'json',
    url: 'api/auth/authenticate',
    data: { email: 'test20@gmail.com', password: 'test20' },
}).done(function(data) {
    console.log(data);

    $.ajax({
        dataType: "json",
        url: "api/bicicletas",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("x-access-token", data.data.token);
        }
    }).done(function(result) {
        console.log(result);
        result.bicicletas.forEach(function (bici) {
        L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    });
});
